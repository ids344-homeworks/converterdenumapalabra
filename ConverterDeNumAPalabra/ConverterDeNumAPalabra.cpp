// IDS344 - 2022-01 - Grupo 1 - Convertedor de monto a palabras
//
// Nikita Kravchenko - 1101607
// Omar N��ez - 1101587
// Oliver Infante - 1100292
// Lervis Pinales - 1096323
// Daniel B�ez - 1073597
//
//  24 de marzo del 2022

#include <iostream>
#include <string>
#include <conio.h>

using namespace std;

void printPart(string cientosS);

void Units(int num);
bool UnitsIncomodos(int num);
void Tens(int num);
void Hundreds(int num);


bool IsNumber(string s)
{
	for (int i = 0; i < s.length(); i++)
		if (isdigit(s[i]) == false)
			return false;

	return true;
}

void Converter()
{
	string EntireNumber;
	string DecimalPart;

	string cientosS = "000";
	string milesS = "000";
	string millonesS = "000";

	int numLength = 0;

	int number, numberDecimal;
	int helper = 0;

	do//Capturar entero
	{
		cout << "La parte entera del monto: \n";
		cin >> EntireNumber;

		numLength = EntireNumber.length();

	} while (!(IsNumber(EntireNumber) && numLength < 10));//aunque not eso

	do //Capturar decimal
	{
		cout << "La parte decimal del monto: \n";
		cin >> DecimalPart;

	} while (!(IsNumber(DecimalPart) && DecimalPart.length() < 3));//aunque not eso

	number = stoi(EntireNumber);//STOI TRANSFORMAR STRING TO INT


	while (numLength < 9) //Llenar string con 0 que no afectan el numero// rellena valores hasta cumplir con la cantidad maxima de digitos
	{
		EntireNumber.insert(0, 1, '0');
		numLength = EntireNumber.length();
	}
	numLength = EntireNumber.length();

	helper = 0;
	for (int i = numLength - 3; i < numLength; i++)//Find part of hundreeds// acomodar los digitos para saber si en unidad decena...etc
	{
		cientosS[helper] = EntireNumber[i];
		helper++;

		EntireNumber[i] = ' ';
	}
	numLength = numLength - 3;

	helper = 0;
	for (int i = numLength - 3; i < numLength; i++)//FInd part of thousands
	{
		milesS[helper] = EntireNumber[i];
		helper++;

		EntireNumber[i] = ' ';
	}

	numLength = numLength - 3;

	helper = 0;
	for (int i = numLength - 3; i < numLength; i++)//FInd part of millones
	{
		millonesS[helper] = EntireNumber[i];
		helper++;

		EntireNumber[i] = ' ';
	}

	cout<< "El numero introducido en palabras es: \n";

	if (number == 0)	
		cout << "ZERO ";	

	printPart(millonesS);
	(stoi(millonesS) == 0) ? cout << "" : (stoi(millonesS) > 1) ? cout << "MILLONES " : cout << "MILLON ";

	printPart(milesS);
	(stoi(milesS) == 0) ? cout << "" : (stoi(milesS) == 1) ? cout << "\b\b\bMIL " : cout << "MIL ";//delete un

	printPart(cientosS);

	(number == 1) ? cout << "PESO CON " : cout << "PESOS CON "; cout << DecimalPart << "/100";
}

int main()
{
	while (true)
	{
		Converter();
		cout << "\n\n";
	}
}

void printPart(string cientosS)
{
	bool wasDe11a19 = false; //Ayuda a ver si hubo decimal de 11 a 19
	bool wasDecimal = false; //Ayuda a ver si hubo decimal para poner y
	bool wasVeinte = false;

	int cientosI = 0;

	cientosI = stoi(cientosS); //Al transformarlo en int eliminamos primer 0 innecesario
	cientosS = to_string(cientosI);

	if (cientosS.length() > 2) //si la longtitud mas que 2 significa que es hundreed
	{
		Hundreds(cientosI);

		cientosS[0] = '0';  //Eliminamos parte de hundreeds
		cientosI = stoi(cientosS);

		cientosS = to_string(cientosI);
	}

	wasDe11a19 = false;
	if (cientosS.length() > 1) //si la longtitud menos que 1 significa que es decimal
	{
		wasDe11a19 = UnitsIncomodos(cientosI); //Was number de 11 a 19?

		if (!wasDe11a19)
		{
			Tens(cientosI);

			wasDecimal = true;

			if (cientosS[0] == '2')
				wasVeinte = true;

			cientosS[0] = '0';  //Eliminamos parte de decimales
			cientosI = stoi(cientosS);

			cientosS = to_string(cientosI);
		}
	}

	if (cientosI != 0)//Falta solo unidad, si hubo decimal, ponemos y
	{
		if (wasVeinte)
			cout << "\b\bI";

		else if (wasDecimal)
			cout << "Y ";

		Units(cientosI);
	}
}

void Units(int num) {
	switch (num) {
	case 0:cout << "CERO "; break;
	case 1:cout << "UN "; break;
	case 2:cout << "DOS "; break;
	case 3:cout << "TRES "; break;
	case 4:cout << "CUATRO "; break;
	case 5:cout << "CINCO "; break;
	case 6:cout << "SEIS "; break;
	case 7:cout << "SIETE "; break;
	case 8:cout << "OCHO "; break;
	case 9:cout << "NUEVE "; break;
	}
}

bool UnitsIncomodos(int num) {
	switch (num) {
	case 11:cout << "ONCE "; break;
		return true;
	case 12:cout << "DOCE "; break;
		return true;
	case 13:cout << "TRECE "; break;
		return true;
	case 14:cout << "CATORCE "; break;
		return true;
	case 15:cout << "QUINCE "; break;
		return true;
	case 16:cout << "DIECISEIS "; break;
		return true;
	case 17:cout << "DIECISIETE "; break;
		return true;
	case 18:cout << "DIECIOCHO "; break;
		return true;
	case 19:cout << "DIECINUEVE "; break;
		return true;
	default:
		return false;
	}
}

void Tens(int num) {
	int tens = (num / 10) % 10;

	switch (tens) {
	case 1:cout << "DIEZ ";
		break;
	case 2:cout << "VEINTE ";
		break;
	case 3:cout << "TREINTA ";
		break;
	case 4:cout << "CUARENTA ";
		break;
	case 5:cout << "CINCUENTA ";
		break;
	case 6:cout << "SECENTA ";
		break;
	case 7:cout << "SETENTA ";
		break;
	case 8:cout << "OCHENTA ";
		break;
	case 9:cout << "NOVENTA ";
		break;
	}
}

void Hundreds(int num) {
	bool isHundreed = false;

	if (num % 100 == 0)
		isHundreed = true;

	int hundreds = (num / 100) % 10;

	switch (hundreds) {
	case 1:(isHundreed) ? cout << "CIEN " : cout << "CIENTO "; break;
	case 2:cout << "DOSCIENTO "; break;
	case 3:cout << "TRESCIENTO "; break;
	case 4:cout << "CUATROCIENTO "; break;
	case 5:cout << "QUINIENTOS "; break;
	case 6:cout << "SEISCIENTO "; break;
	case 7:cout << "SETECIENTO "; break;
	case 8:cout << "OCHOCIENTO "; break;
	case 9:cout << "NOVECIENTO "; break;
	}
}